#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
imu_listener_node.py: interfacing with the drone's imu

"""

###############################################
# Standard Imports                            #
###############################################
import numpy as np

###############################################
# ROS Imports                                 #
###############################################
import rclpy
from rclpy.node import Node

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String
from sensor_msgs.msg import Imu

# Function to convert quaternion to roll, pitch, yaw in dregees
def quaternion_to_euler_angle(w, x, y, z):
    ysqr = y * y

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + ysqr)
    X = np.degrees(np.arctan2(t0, t1))

    t2 = +2.0 * (w * y - z * x)

    t2 = np.clip(t2, a_min=-1.0, a_max=1.0)
    Y = np.degrees(np.arcsin(t2))

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = np.degrees(np.arctan2(t3, t4))

    return X, Y, Z

###############################################
# IMU Listener class                          #
###############################################
class ImuListener(Node):

    def __init__(self):
        # Init the node and call it: imu_listener_node
        super().__init__('imu_listener_node')
        print("Starting", self.get_name(),"\n") # get the name of the node

        # Important to setup the Quality of Service (QoS) policy to be compatible with the publisher
        # to check the publishers QoS policy use:
        # ros2 topic info /topic_name --verbose
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT, history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1)

        # Create a subscriber to listen on the '/mavros/imu/data' topic and specify the message as Imu with a callback and the correct QoS policy
        self.imu_listener_sub = self.create_subscription(Imu, '/mavros/imu/data',self.listener_callback, qos_policy)

    def listener_callback(self, msg):
        # Get the timestamp
        print("Timestamp:",msg.header.stamp.sec)

        # Get roll, pitch, yaw
        (X, Y, Z) = quaternion_to_euler_angle(msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w) # outputs in degrees
        print("Roll:", X)
        print("Pitch:", Y)
        print("Yaw:", Z)

        print("Angular velocity in roll",msg.angular_velocity.x)
        print("Angular velocity in pitch",msg.angular_velocity.y)
        print("Angular velocity in yaw",msg.angular_velocity.z)

        print("Linear acceleration in m/s2[x]:", msg.linear_acceleration.x)
        print("Linear acceleration in m/s2[y]:", msg.linear_acceleration.y)
        print("Linear acceleration in m/s2[z];", msg.linear_acceleration.z)
        print("")

def main(args=None):
    rclpy.init(args=args)

    imu_listener = ImuListener()

    rclpy.spin(imu_listener)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    imu_listener.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

'''
Timestamp: 1667812537
Roll: -3.4570121984163484
Pitch: -0.9039187407205692
Yaw: -179.9082098532944
Angular velocity in roll -0.0015851606149226427
Angular velocity in pitch 0.0016315475804731248
Angular velocity in yaw -0.002305209869518876
Linear acceleration in m/s2[x]: -0.1622176468372345
Linear acceleration in m/s2[y]: -0.019231652840970746
Linear acceleration in m/s2[z]; 9.806700706481934
'''