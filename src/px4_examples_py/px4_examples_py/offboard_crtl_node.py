#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
offboard_ctrl.py: Controlling the setpoints

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
import math

###############################################
# ROS Imports                                 #
###############################################
import rclpy
from rclpy.node import Node

###############################################
# ROS Topic messages                          #
###############################################
#from std_msgs.msg import String
#from std_msgs.msg import Float32


from geometry_msgs.msg import PoseStamped
from mavros_msgs.msg import State
from sensor_msgs.msg import Imu
###############################################
# ROS Service messages                        #
###############################################

from mavros_msgs.srv import CommandBool, SetMode

###############################################
# Offboad Control class                       #
###############################################
class OffboardControl(Node):

    def __init__(self):
        super().__init__('offboard_ctrl_node')
        self.publisher_ = self.create_publisher(PoseStamped, '/mavros/setpoint_position/local', 10)
        print("Starting Node")
        qos_policy = rclpy.qos.QoSProfile(reliability=rclpy.qos.ReliabilityPolicy.BEST_EFFORT, history=rclpy.qos.HistoryPolicy.KEEP_LAST, depth=1)
        self.state_sub = self.create_subscription(Imu, '/mavros/imu/data',self.state_cb, qos_policy)
        
        #rclpy.qos.ReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT)
        #self.state_sub


        timer_period = 0.25  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0
        self.msg = PoseStamped()
        self.msg.header.frame_id = "map"

    def state_cb(self, msg):
        global current_state
        current_state = msg
        print(current_state)

    def timer_callback(self):
        x = 2.0 * math.cos(self.i*2*math.pi/360)
        y = 2.0 * math.sin(self.i*2*math.pi/360)
        z = 2.0

        self.msg.pose.position.x = x
        self.msg.pose.position.y = y
        self.msg.pose.position.z = z


        self.i = self.i + 1

        # Reset counter
        if(self.i > 360):
            self.i = 0
        
        self.publisher_.publish(self.msg)
        
        #self.get_logger().info('Publishing: "%s"' % msg)
        #self.i += 1


# MANGLER AT TILFOJE AT ARM DRONE

# MANGLER AT AENDRE STATE TIL OFFBOARD


def main(args=None):
    rclpy.init(args=args)

    offb_ctrl = OffboardControl()

    rclpy.spin(offb_ctrl)
    offb_ctrl.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
